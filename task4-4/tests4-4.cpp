//
// Created by user on 4/21/21.
//
#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include <iostream>
#include <string>
#include <ctime>
#include <fstream>
#include "task4-4.h"
TEST_CASE("Simple","[Normal_Work]")
{
   const int size=10;
   std::ifstream in("input.txt");
        int ab[size];
        int ac[size];
        int ad[size];
        int ae[size];
        int af[size];
        int ag[size];
        for (int i = 0; i <size ; ++i)
        {
                in>>ab[i];//обычный массив
                ag[i]=ab[i];
        }
        for (int i = 0; i <size ; ++i)
        {
                in>>ac[i];//массив одинаковых значений
        }
        for (int i = 0; i <size ; ++i)
        {
            in>>ad[i];// массив значений c повторениями
        }
        for (int i = 0; i <size ; ++i)
        {
             in>>ae[i];// неубывающий массив
        }
        for (int i = 0; i <size ; ++i)
        {
            in>>af[i];// невозврастающий массив
        }
    CHECK(!CorrectArray(ab,size));
    CHECK(CorrectArray(ac,size));
    CHECK(!CorrectArray(ad,size));
    CHECK(CorrectArray(ae,size));
    CHECK(!CorrectArray(af,size));
    CHECK_NOTHROW(HeapSort(ab,size));
    CHECK_NOTHROW(HeapSort(ac,size));
    CHECK_NOTHROW(HeapSort(ad,size));
    CHECK_NOTHROW(HeapSort(ae,size));
    CHECK_NOTHROW(HeapSort(af,size));
    for (int j = 0; j <size-1 ; ++j)
    {
        CHECK(ab[j]<=ab[j+1]);
        CHECK(ac[j]<=ac[j+1]);
        CHECK(ad[j]<=ad[j+1]);
        CHECK(ae[j]<=ae[j+1]);
        CHECK(af[j]<=af[j+1]);
    }
    CHECK(CorrectArray(ab,size));
    CHECK(CorrectArray(ac,size));
    CHECK(CorrectArray(ad,size));
    CHECK(CorrectArray(ae,size));
    CHECK(CorrectArray(af,size));

    for (int k = 0; k <size ; ++k)
    {
        for (int i = k; i <size ; ++i)
        {
            if(ag[i]<ag[k])
                std::swap(ag[i],ag[k]);
        }
    }
    CHECK(sizeof(af)/sizeof(int)==size);
    for (int j = 0; j <size ; ++j)
    {
        CHECK(ab[j]==ag[j]);//проверка того что данные не изменлись
    }
    CHECK(sizeof(ab)/sizeof(int)==size);
    CHECK(sizeof(ac)/sizeof(int)==size);
    CHECK(sizeof(ae)/sizeof(int)==size);
    CHECK(sizeof(af)/sizeof(int)==size);
    CHECK(sizeof(ag)/sizeof(int)==size);//проверка того что до и после сортировки колличество элементов в массивах не изменилось
    in.close();
}

TEST_CASE("Big_Array","[Normal_Work]")
{
    srand(time(0));
    int size=200;
    int arr[size];
    for (int i = 0; i < size; ++i)
    {
        arr[i] = rand() % 100;
    }
    CHECK_NOTHROW(HeapSort(arr,size));
    CHECK(CorrectArray(arr,size));
}

TEST_CASE("HeapSort_Sort_Positive_And_Negative","[Normal_Work]")
{
        std::ifstream in("input3.txt");
        int *arr=new int ;
        int i=0;
        for(;!in.eof();++i)
        {
            in>>arr[i];

        }
        CHECK_NOTHROW(HeapSort(arr, i));
        CHECK(CorrectArray(arr, i));
        in.close();

}

TEST_CASE("Exept_arr_Zero","[Exept_Work]") {
    int arr[0];
    CHECK(CorrectArray(arr, 0));
    CHECK_NOTHROW(HeapSort(arr, 0));
    CHECK(CorrectArray(arr, 0));
}

TEST_CASE("Exept_arr_OneElem","[Exept_Work]") {
    std::ifstream in("input2.txt");
    int arr[1];
    in>>arr[0];
    CHECK(CorrectArray(arr, 1));
    CHECK_NOTHROW(HeapSort(arr, 1));
    CHECK(CorrectArray(arr, 1));
    in.close();
}

