//
// Created by user on 4/21/21.
//
#include "task4-4.h"
void StructurePyramyd( int& arr,   int SizeArr)
{
    int i = SizeArr / 2;
    while (i >= 0)/*Цикл необходимый для перебора всех ветвей имеющих сыновей */
    {
        RestructNumbers(&arr, i, SizeArr); /*Функция используется для перестановки значений*/
        --i;
    }
}
void RestructNumbers( int* array,  int i,  int n)
{
    if (2 * i + 2/**/ < n) /*Проверочное условие для того чтобы избежать ошибку обращения к памяти вне массива*/
    {
        if (array[2 * i + 1] >= array[2 * i + 2] && array[2 * i + 1] > array[i])/*истинность условного оператора обозначает что значение в левое
																				ответвление родительского значения нуждается в перестановке */
        {
            std::swap(array[2 * i + 1], array[i]);/*используется для сокращения колличества строк кода и увеличения наглядности реализации сортировки*/
            RestructNumbers(array, 2 * i + 1, n);/*Рекурентный вызов необходим для того чтобы поставить элеиент массива на
												 правильное место помогает скоратить колличество итераций по всему массиву */
        }
        if (array[2 * i + 1] <= array[2 * i + 2] && array[2 * i + 2] > array[i])/*истинность условного оператора обозначает что значение в правое
																				ответвление родительского значения нуждается в перестановке */
        {
            std::swap(array[2 * i + 2], array[i]);
            RestructNumbers(array, 2 * i + 2, n);
        }
    }
    else
        if(2*i+1<n)
        {
            if(array[2*i+1]>array[i])
                std::swap(array[2*i+1],array[i]);
        }
}

void SortingProcess( int& arr, int SizeArr)/*в функции происходит перестановка корневого значения и последнего в массиве
													  затем фиксация этого значения и просеивание элементов через пирамиду за исключением
													  зафиксированного значения */
{
    --SizeArr;/*способ фиксации крайнего значения*/
    if (SizeArr > 1)
    {
        std::swap(arr, *(&arr + SizeArr));
        StructurePyramyd(arr, (SizeArr));
        SortingProcess(arr, SizeArr);
    }
    else if (arr > *(&arr + 1)) std::swap(arr, *(&arr + 1)); /*необходимо для пограничной ситуации с двумя последними
															 значениями(последними по порядку сортировки, а не по индексу)*/
}
void HeapSort(int *arr,int SizeArr)
{
    if(SizeArr>=2)
    {
        StructurePyramyd(*arr, SizeArr);
        SortingProcess(*arr, SizeArr);
    }
}
bool CorrectArray(int *arr,int SizeArr)
{
    for (int i = 0; i <SizeArr-1 ; ++i) {

        if(arr[i]>arr[i+1])
            return 0;

    }
    return 1;

}