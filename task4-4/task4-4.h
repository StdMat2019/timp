//
// Created by user on 4/21/21.
//
#ifndef TEST_EMPTY_TASK4_4_H
#define TEST_EMPTY_TASK4_4_H
//Алгоритм пирамидальной сортировки.
#include<iostream>
void StructurePyramyd(  int& array, int SizeArr); /*Функция используемая для построения
															 пирамиды,  в которой вершиной является значение самой большой переменной массива*/
void RestructNumbers(int* array, int i, int n); /*Вспомогательная функция для элементарной перестановки значений*/
void SortingProcess(int& arr,int SizeArr); /*Функция построения отсортированной пирамиды  */
void HeapSort(int *arr,int SizeArr);
bool CorrectArray(int *arr,int SizeArr);

#endif //TEST_EMPTY_TASK4_4_H
