
/*Стек на основе связного списка. Операции: добавление элемента; извлечение элемента; определение количества
 * элементов в стеке; очистка стека.*/
#ifndef TASK_4_1_TASK_4_1_H
#define TASK_4_1_TASK_4_1_H
#pragma once
#include <iostream>
#include <string>
struct Knot
{
    int Value;
    Knot *nextElem;
    Knot(int Value){this->Value=Value; nextElem= nullptr;}
    ~Knot(){};
};
class LinkedList
{
private:
    Knot *phead;
    int SizeList;
public:
    LinkedList();
    ~LinkedList();
    void Push_Back(int value);
    int Pop_Back();
    void Clear();
    int GetSize();
};
class ListStack
{
private:
    LinkedList *MyStack;
public:
    ListStack();
    ~ListStack();
    void Push(int value);
    void Clean();
    int Pop();
    int GetSizeStack();
};
/*
class Exept
{
private:
    int codeError;
    std::string errorText;
public:
    Exept(int codeError)
    {
        this->codeError=codeError;
        switch(codeError)
        {
            case 1:
                codeError=1;
                errorText="You can't clean empty stack";
                break;
            case 2:
                codeError=2;
                errorText="You can't delete element of empty stack";
                break;

            case 3:
                codeError=3;
                errorText="You can not delete element of empty list";
                break;
            case 4:
                codeError=4;
                errorText= "You can not clean empty list" ;
                break;

        }
    }

};
 */
#endif //TASK_4_1_TASK_4_1_H
