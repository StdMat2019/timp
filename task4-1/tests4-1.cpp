#define CATCH_CONFIG_MAIN
#include "task4-1.h"
#include <iostream>
#include "../catch.hpp"
#include <ctime>
TEST_CASE("ListNoThrow","[Test_NO_Exert_List]")
{
    LinkedList Simple;
    CHECK_NOTHROW(Simple.GetSize());
    CHECK_NOTHROW(Simple.Push_Back(432));
    CHECK_NOTHROW(Simple.Pop_Back());
    CHECK_NOTHROW(Simple.Push_Back(-6565));
    CHECK_NOTHROW(Simple.Clear());

}

TEST_CASE("ListThrow","[Test_Exert_List]")
{
    LinkedList Simple;
    CHECK(Simple.GetSize()==0);
    CHECK_THROWS(Simple.Pop_Back());
    CHECK_NOTHROW(Simple.Clear());
    CHECK(0==Simple.GetSize());
}

TEST_CASE("ListWork","[Test_List]")
{
    srand(time(0));
    LinkedList Simple;
    int value;
    for (int i = 0; i <5 ; ++i)
    {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push_Back(value));
        CHECK(i+1==Simple.GetSize());
    }
    for (int i = 5; i >0 ; --i)
    {
        CHECK(i==Simple.GetSize());
        CHECK_NOTHROW(Simple.Pop_Back());
    }
    CHECK(0==Simple.GetSize());
}
TEST_CASE("ListWorkClean","[Test_List]")
{
    srand(time(0));
    LinkedList Simple;
    int value;
    for (int i = 0; i <5 ; ++i)
    {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push_Back(value));
        CHECK(i+1==Simple.GetSize());
    }
    CHECK_NOTHROW(Simple.Clear());
    CHECK(0==Simple.GetSize());
}
TEST_CASE("Test_Clean_Stack","[Test_Func]")
{
    srand(time(0));
    ListStack Simple;
    int value;
    for (int i = 0; i <5 ; ++i) {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push(value));
        CHECK(i+1==Simple.GetSizeStack());
    }
    CHECK_NOTHROW(Simple.Clean());
    CHECK(Simple.GetSizeStack()==0);
}
TEST_CASE("Test_Clean_After_Clean","[Test_Exept]")
{
    srand(time(0));
    ListStack Simple;
    CHECK_NOTHROW(Simple.Push(rand()%1000000000));
    CHECK_NOTHROW(Simple.Clean());
    CHECK_NOTHROW(Simple.Clean());
}
TEST_CASE("Test_Clean_In_Empty","[Test_Exept]")
{
    srand(time(0));
    ListStack Simple;
    CHECK_NOTHROW(Simple.Clean());
}
TEST_CASE("Test_Clean_After_Pop","[Test_Exept]")
{
    srand(time(0));
    ListStack Simple;
    CHECK_NOTHROW(Simple.Push(rand()%1000000000));
    CHECK_NOTHROW(Simple.Pop());
    CHECK_NOTHROW(Simple.Clean());
}
TEST_CASE("Test_Push","[Test_Func]")
{
    srand(time(0));
    ListStack Simple;
    int value;
    int ValueArray[10];
    for (int i = 0; i <10 ; ++i) {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push(value));
        ValueArray[i]=value;
        CHECK(i+1==Simple.GetSizeStack());
    }
    for (int i = 9; i <=0 ; --i)
    {
        CHECK(ValueArray[i]==Simple.Pop());
    }
}
TEST_CASE("Test_Push_ReSize","[Test_Func]")
{
    srand(time(0));
    ListStack Simple;
    int value;
    int ValueArray[10];
    for (int i = 0; i <10 ; ++i) {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push(value));
        ValueArray[i]=value;
        CHECK(i+1==Simple.GetSizeStack());
    }
    for (int i = 9; i <=0 ; --i)
    {
        CHECK(i==Simple.GetSizeStack());
        CHECK(ValueArray[i]==Simple.Pop());
    }
}
TEST_CASE("Test_Push_After_Clean","[Test_Func]")
{
    srand(time(0));
    ListStack Simple;
    int value;
    value=rand()%1000000000;
    CHECK_NOTHROW(Simple.Push(value));
    CHECK_NOTHROW(Simple.Clean());
    int ValueArray[10];
    for (int i = 0; i <10 ; ++i) {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push(value));
        ValueArray[i]=value;
    }
    for (int i = 9; i <=0 ; --i)
    {
        CHECK(ValueArray[i]==Simple.Pop());
    }
}

TEST_CASE("Test_Pop_All_Elem","[Test_Func]")
{
    srand(time(0));
    ListStack Simple;
    int value;
    for (int i = 0; i <5 ; ++i) {
        value=rand()%1000000000;
        CHECK_NOTHROW(Simple.Push(value));
    }
    for (int i = 5; i >1 ; --i)
    {
        CHECK(Simple.GetSizeStack()==i);
        CHECK_NOTHROW(Simple.Pop());
    }
    CHECK_NOTHROW(Simple.Clean());
}

TEST_CASE("Test_Pop_Exept_Empty","[Test_Exept]")
{
    srand(time(0));
    ListStack Simple;
    CHECK_THROWS( Simple.Pop());
}
TEST_CASE("Test_Pop_Exept_After_Push","[Test_Exept]")
{
    srand(time(0));
    ListStack Simple;
    int value=rand()%1000000000;
    CHECK_NOTHROW(Simple.Push(value));
    CHECK_NOTHROW( Simple.Pop());
    CHECK_THROWS( Simple.Pop());
}
TEST_CASE("Test_Pop_Exept_After_Clean","[Test_Exept]")
{
    srand(time(0));
    ListStack Simple;
    int value=rand()%1000000000;
    CHECK_NOTHROW(Simple.Push(value));
    CHECK_NOTHROW( Simple.Clean());
    CHECK_THROWS( Simple.Pop());
}

