#include"task4-1.h"
#include <string>
ListStack::ListStack()
{
    MyStack= nullptr;
}
ListStack::~ListStack() {
    if (nullptr != MyStack)
    {
        while (0 != MyStack->GetSize()) {
            MyStack->Pop_Back();
        }
   delete MyStack;
}
}
void ListStack::Push(int value) {
    if (nullptr == MyStack) {
        MyStack = new LinkedList;
    }
        MyStack->Push_Back(value);

}
void ListStack::Clean()
{
   // try{
        if (nullptr != MyStack)
        {
            MyStack->Clear();
        }


    /*}
    catch (const int  )
    {

        Exept(1);
        //std::cout<<"You can't clean empty stack"<<std::endl;
    }*/
}
int ListStack::Pop()
{
   // try{
        if (nullptr==MyStack )
        {
            throw ("You can't delete element of empty stack");
        }
        return MyStack->Pop_Back();
   /* }
    catch (const int )
    {
        Exept(2);
        //std::cout<<"You can't delete element of empty stack"<<std::endl;
    }*/
}
int ListStack::GetSizeStack()
{
    return MyStack->GetSize();
}
LinkedList::LinkedList()
{
    phead= nullptr;
    SizeList=0;
}
LinkedList::~LinkedList()
{
    while (0 != SizeList)
    {
        this->Pop_Back();
    }
}
void LinkedList::Push_Back(int value)
{
    int a;
    if(nullptr==this->phead)
    {
        phead= new Knot(value);
        ++SizeList;
    }
    else
    {
        Knot* iter= phead;
        while (nullptr!=iter->nextElem)
        {
            iter=iter->nextElem;
        }

        iter->nextElem=new Knot(value);
        ++SizeList;
    }
}
int LinkedList::Pop_Back()
{
   // try {
        if (nullptr==this->phead) {
            throw ("You can not delete element of empty list");
        }
        Knot *iter = phead;
        if (nullptr == iter->nextElem) {
            delete iter;
            phead= nullptr;
            --SizeList;
        }
        else {
            while (nullptr != iter->nextElem->nextElem) {
                iter = iter->nextElem;
            }
            delete iter->nextElem;
            iter->nextElem = nullptr;
            --SizeList;
        }
   /* }
    catch(const int )
    {
        Exept(3);
       // std::cout<<"You can not delete element of empty list" <<std::endl;
    }*/
}
int LinkedList::GetSize()
{
    return SizeList;
}
void LinkedList::Clear()
{
    //try{
        /*if (nullptr == this->phead)
        {
            throw ( "You can not clean empty list");
        }*/
        while (nullptr != this->phead)
        {
            this->Pop_Back();
        }
    /*}
    catch (const int)
    {
        Exept(4);
        //std::cout << "You can not clean empty list" << std::endl;
    }*/
}