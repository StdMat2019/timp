//
// Created by user on 4/16/21.
//
/*Хеш-таблица с разрешением коллизий с помощью связных списков.
 * Операции:
 * добавление элемента по ключу;
 * поиск элемента по ключу;
 * определение количества элементов списка для заданной ячейки хэш-таблицы;
 * определение общего количества содержащихся элементов.*/
#ifndef TEST_EMPTY_TASK4_3_H
#define TEST_EMPTY_TASK4_3_H

#include <string>
#include <utility>
#include <list>
#include <iostream>

const int sizeTable=100;
class Hash
        {
        private:
            std::list<std::pair<long,std::pair<long,std::string>>  > *valueList;
        public:
            Hash();
            ~Hash();
            void Push(long keyPair,std::string value);
            std::string SearchValue(long key);
            int NumberOfCollisionElem();
            int NumberInTable();
            long NumberInList(int i);
            void Print();
            friend long HashFunc(long keyPair);
        };

long HashFunc(long keyPair);



#endif //TEST_EMPTY_TASK4_3_H

