//
// Created by user on 4/16/21.
//
#define CATCH_CONFIG_MAIN
#include "task4-3.h"
#include "../catch.hpp"
#include <iostream>
#include <string>

TEST_CASE("Simple_No_NOThrow","[Normal_Work]")
{
    Hash TableA;
    CHECK_NOTHROW(TableA.NumberOfCollisionElem());
    CHECK_NOTHROW(TableA.NumberInTable());
    CHECK_NOTHROW(TableA.NumberInList(HashFunc(1000)%100));//
    CHECK_NOTHROW(TableA.SearchValue(1234));
    CHECK_NOTHROW(TableA.Push(1234,"Value"));
    CHECK_NOTHROW(TableA.Push(13455,"Value1"));
    CHECK_NOTHROW(TableA.Push(2323335435,"Value"));
    CHECK_NOTHROW(TableA.Push(68676,"Value4"));
    CHECK_NOTHROW(TableA.Push(6867878,"Value6"));
    CHECK_NOTHROW(TableA.Push(224424244,"Value88"));
    CHECK_NOTHROW(TableA.NumberInList(HashFunc(1000)%100));//
    CHECK_NOTHROW(TableA.SearchValue(1234));
    CHECK_NOTHROW(TableA.SearchValue(13455));
    CHECK_NOTHROW(TableA.SearchValue(2323335435));
    CHECK_NOTHROW(TableA.NumberInList(HashFunc(1000)%100));//
    CHECK_NOTHROW(TableA.SearchValue(2323335499999));
    CHECK_NOTHROW(TableA.NumberOfCollisionElem());
    CHECK_NOTHROW(TableA.NumberInTable());
    CHECK_NOTHROW(TableA.NumberInList(HashFunc(1000)%100));//
}

TEST_CASE("Simple_Types_Of_Collisions","[Normal_Work]")
{
    Hash TableA;
    CHECK_NOTHROW(TableA.Push(23454,"Value"));
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(1==TableA.NumberInList(HashFunc(23454)%100));//
    CHECK("Value"==TableA.SearchValue(23454));
    CHECK_NOTHROW(TableA.Push(876,"Value4")); //коллизия с разным хешем
    CHECK("Value4"==TableA.SearchValue(876)); //поиск в листе с двумя значениями
    CHECK(2==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(2==TableA.NumberInList(HashFunc(876)%100));//
    CHECK_NOTHROW(TableA.Push(876,"Value4")); //коллизия с разным хешем
    CHECK("Value4"==TableA.SearchValue(876)); //поиск в листе с двумя значениями
    CHECK(2==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(2==TableA.NumberInList(HashFunc(876)%100));//
    CHECK_NOTHROW(TableA.Push(34556,"Value88")); //совпадение значений хеша
    CHECK("Value88"==TableA.SearchValue(34556));
    CHECK("Value4"!=TableA.SearchValue(876));//значение перезаписалось из-за одинаковых значений хеша
    CHECK(2==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(2==TableA.NumberInList(HashFunc(34556)%100));//
}

TEST_CASE("Regular_Work","[Normal_Work]")
{
    Hash TableA; //элементы без коллизий
    CHECK(0==TableA.NumberOfCollisionElem());
    CHECK(0==TableA.NumberInTable());
    CHECK(0==TableA.NumberInList(0));//
    CHECK_NOTHROW(TableA.Push(00007654,"Value1"));
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(1==TableA.NumberInList(HashFunc(0007654)%100));//
    CHECK("Value1"==TableA.SearchValue(00007654));
    CHECK_NOTHROW(TableA.Push(1077700,"Value2"));
    CHECK(2==TableA.NumberOfCollisionElem());
    CHECK(2==TableA.NumberInTable());
    CHECK(1==TableA.NumberInList(HashFunc(1077700)%100));//
    CHECK("Value2"==TableA.SearchValue(1077700));
    CHECK_NOTHROW(TableA.Push(10311111,"Value3"));
    CHECK(3==TableA.NumberOfCollisionElem());
    CHECK(3==TableA.NumberInTable());
    CHECK(1==TableA.NumberInList(HashFunc(10311111)%100));//
    CHECK("Value3"==TableA.SearchValue(10311111));
    CHECK_NOTHROW(TableA.Push(65913767483,"Value4"));
    CHECK(4==TableA.NumberOfCollisionElem());
    CHECK(4==TableA.NumberInTable());
    CHECK("Value4"==TableA.SearchValue(65913767483));

}

TEST_CASE("Similar_HashFunc","[Normal_Work]")
{
    Hash TableA;//одинаковые хеши
    CHECK_NOTHROW(TableA.Push(10000,"Value1"));
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(1==TableA.NumberInList(HashFunc(1000)%100));//
    CHECK(0==TableA.NumberInList(50));
    CHECK(0==TableA.NumberInList(3));
    CHECK_NOTHROW(TableA.Push(34000,"Value2"));
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK_NOTHROW(TableA.Push(766000,"Value3"));
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK(1==TableA.NumberInList(HashFunc(766000)%100));//
    CHECK_NOTHROW(TableA.Push(54545000,"Value4"));
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK_NOTHROW(TableA.Push(156645000,"Value5"));
    CHECK(1==TableA.NumberInList(HashFunc(156645000)%100));//
    CHECK(1==TableA.NumberOfCollisionElem());
    CHECK(1==TableA.NumberInTable());
    CHECK("Value5"==TableA.SearchValue(156645000));
    CHECK("Value1"!=TableA.SearchValue(10000));
    CHECK("Value2"!=TableA.SearchValue(34000));
    CHECK("Value3"!=TableA.SearchValue(766000));
    CHECK("Value4"!=TableA.SearchValue(54545000));
}

TEST_CASE("Simple_Empty","[Normal_Work]")
{
    Hash TableA;
    CHECK(0==TableA.NumberOfCollisionElem());
    CHECK(0==TableA.NumberInTable());
    CHECK_NOTHROW(TableA.SearchValue(342342));
}
TEST_CASE("Simple_Empty_Value","[Normal_Work]")
{
    Hash TableA;
    CHECK(0==TableA.NumberOfCollisionElem());
    CHECK(0==TableA.NumberInTable());
    CHECK_THROWS(TableA.Push(10000,""));
    CHECK(0==TableA.NumberInList(50));
    CHECK(0==TableA.NumberInList(3));
    CHECK_NOTHROW(TableA.NumberInList(1));//
    CHECK_NOTHROW(TableA.SearchValue(342342));
}