//
// Created by user on 4/16/21.
//
#include "task4-3.h"
#include <string>
long HashFunc(long keyPair)
{
    long int key=0;
    for (int i = 0; i <10 ; ++i)
    {
        key+=keyPair%10*i;
    }
    key=(key%500)*((keyPair)%20);
    return key;
}
Hash::Hash()
{
     valueList =new std::list<std::pair<long,std::pair<long,std::string>>  > [sizeTable];
}

Hash::~Hash()
{
    delete [] valueList;
}

void Hash::Push(long keyPair,std::string value)
{
    if(value=="") throw ("Попытка добавления пустого значения");
   long key = HashFunc(keyPair);
   for( auto &iter: *(valueList+key % sizeTable))
   {
       if(iter.first==key)
       {
           iter.second.first=keyPair;
           iter.second.second=value;
           return ;
       }
   }
   std::pair<long,std::string> a(keyPair,value);
   (valueList + key % sizeTable)->push_front(std::pair<long,std::pair<long,std::string>> (key,a));
}

std::string Hash::SearchValue(long keyPair)
{
    long key=HashFunc(keyPair);
    for( auto &iter: *(valueList+key % sizeTable))
    {
        if (iter.first == key)
        {
            std::pair<long,std::string> a(iter.second.first,iter.second.second);
            return iter.second.second;
        }
    }
    return "";
}

int Hash::NumberOfCollisionElem() {
    int sizeCollizion=0;
    for (int i = 0; i < sizeTable; ++i) {
       sizeCollizion+= valueList[i].size();
    }
    return sizeCollizion;
}

int Hash::NumberInTable() {
    int sizeElem=0;
    for (int i = 0; i < sizeTable; ++i) {
        if(valueList[i].size()>0)
        ++sizeElem;
    }
    return sizeElem;
}

void Hash::Print() {
        for (int i = 0; i <sizeTable ; ++i)
        {
            if( 0!=(valueList+i)->size())
            {
                std::cout << i<<'\t';
                for(auto iter : *(valueList+i) ) {
                    std::cout << iter.first << ' ' << iter.second.first<<' '<<iter.second.second << ' ';
                }
            }
            else {
                std::cout << i<<"\t0 0 ";
            }
            std::cout<<"\n";
        }


}

long Hash::NumberInList(int i) {
    return (valueList+i)->size();
}
