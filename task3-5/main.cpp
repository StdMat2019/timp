
#include <iostream>
#include "task3-5.h"
#include <string>
#define CATCH_CONFIG_MAIN
#include "../catch.hpp"

int main()
{
    //home/user/Desktop/timp/data/input.txt
    setlocale(LC_ALL, "ru");
    std::string InpFileName, OutFileName;

    std::string key;
    int choice = 1;
    while (choice != 0) {
        std::cout << "\nВыберите функцию:\n 1-копирование файла(Copy)\n 2- сумма чисел хранящихся в файл(Sum)\n 3-шифрование(Shifr)\n 0-окончание работы\n ";
        std::cin >> choice;
        switch (choice)
        {
            case 1:
                std::cout << "Введите название входного файла:\n";
                std::cin >> InpFileName;
                std::cout << "Введите название итогового файла:\n";
                std::cin >> OutFileName;
                std::cout << "Результат функции Copy: " << Copy(InpFileName, OutFileName) << " строк(и)\n";
                break;
            case 2:
                std::cout << "Введите название входного файла:\n";
                std::cin >> InpFileName;
                std::cout << "Результат функции Sum: " << Sum(InpFileName) << "\n";
                break;
            case 3:

                std::cout << "Введите название входного файла:\n";
                std::cin >> InpFileName;
                std::cout << "Введите название итогового файла:\n";
                std::cin >> OutFileName;
                std::cout << "Введите код:\n";
                std::cin >> key;
                std::cout << "Результат функции Shifr: " << Shifr(InpFileName, OutFileName, key) << " Байт\n";
                break;
            case 0:
                std::cout << "Работа закончена\n";
                break;
            default:
                std::cout << "Неверное значение попробуйте еще рез\n";
                break;
        }
    }
}
