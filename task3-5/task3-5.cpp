#include "task3-5.h"
#include <string>
#include <fstream>
#include <iostream>

int Copy(std::string InpFileName, std::string OutFileName)
{
    std::ifstream inp(InpFileName,std::ios::app);
    if (!inp.is_open()) {
        std::cout << "Файл из которого должно осуществляться чтение не открыт!"; exit;
    }
    else
        std::cout << "Файл из которого должно осуществляться открыт\n";
    std::ofstream outf(OutFileName);
    std::string text;
    int count = -1;
    while (inp)
    {
        std::getline(inp, text);
        outf << text << "\n";
        count++;
    }
    return count;
}

float Sum(std::string InpFileName)
{
    std::ifstream inp;
    inp.open(InpFileName);
    if (!inp.is_open()) {
        std::cout << "Файл не открыт!\n"; exit;
    }
    else std::cout << "Файл открыт\n";
    float InA;
    float SuMM = 0;
    while (!inp.eof())
    {
        inp >> InA;
        SuMM += InA;
        InA=0;
    }
    return SuMM;
}

int Shifr(std::string InpFileName, std::string OutFileName, std::string key)
{
    std::ifstream InpF(InpFileName);
    if (!InpF.is_open()) {
        std::cout << "Файл не открыт!\n"; exit;
    }
    else std::cout << "Файл открыт\n";
    std::ofstream outf(OutFileName);
    int NumBit = 0;
    while (InpF) {
        std::string text;

        std::getline(InpF, text);
        for (int i = 0; text[i] != '\0'; i++)
        {
            text[i] = text[i] ^ key[i % key.size()];
            NumBit++;
        }
        outf << text << "\n";
    }
    return NumBit;

}
bool Correct(std::string enter_file_name, std::string result_file_name)
{
    std::ifstream inp1(enter_file_name,std::ios::app);
    if (!inp1.is_open()) {
        std::cout << "Файл из которого должно осуществляться чтение не открыт!"; exit;
    }
    else
        std::cout << "Файл из которого должно осуществляться открыт\n";
    std::ifstream inp2(result_file_name,std::ios::app);
    if (!inp2.is_open()) {
        std::cout << "Файл в который должно осуществляться чтение не открыт!"; exit;
    }
    else
        std::cout << "Файл в который должно осуществляться открыт\n";
    std::string text1;
    std::string text2;
    while (inp1)
    {
        std::getline(inp1, text1);
        std::getline(inp2, text2);
        if(text1!=text2)
            return 0;
    }

     return 1;

}
