#include "iostream"
#include "cstdio"
#include <iostream>
#include "task3-5.h"
#include <string>
#define CATCH_CONFIG_MAIN
#include "../catch.hpp"


TEST_CASE("Fist_rignt_reading_test","[Copy]")
{
    CHECK(Correct("f1_in_correct.txt","f1_in_correct.txt")==true); //сранвнение с самим собой

}
TEST_CASE("empty_reading_test","[Copy]")
{
    CHECK(Copy("empty.txt","f2_in_correct.txt")>=0); //

}
TEST_CASE("_copy_test","[Copy]")
{
    CHECK(Copy("f1_in_correct.txt","f2_in_correct1.txt")>=0);//разные файлы
    CHECK(Correct("f1_in_correct.txt","f2_in_correct.txt")==true);
}
TEST_CASE("Fist_unrignt_copy_test","[Copy]")
{
    CHECK(Copy("f1_in_correct.txt","f1_in_correct.txt")<=0); //копироание самого в себя
    CHECK(Correct("f1_in_correct.txt","f1_in_correct.txt")==true); //проверка
}
TEST_CASE("test_empty","testgroup1")
{
    CHECK(Sum("empty.txt")==0.0f);//проверка на пустой файл


}
TEST_CASE("test_Sum", "testgroup1")
{
    CHECK(Sum("SumTest.txt") == 9.0f);//проверка суммы
}
TEST_CASE("Sum2","testgroup1")
{
    CHECK(Sum("SUMTEST.txt")==12.0f);//проверка на пустой файл


}
TEST_CASE("Fist_rignt_Shif_test","[Copy]")
{
    CHECK(Shifr("f1_in_correct.txt","SHIF.txt","656565")==0); //проерка работы шифования
    CHECK(Correct("f1_in_correct.txt","SHIF.txt")==false);//проверка работы ключа
}
TEST_CASE("Fist_rignt_Scopy_test","[Copy]")
{
    CHECK(Shifr("f1_in_correct.txt","SHIF.txt","656565")==0);
    CHECK(Shifr("SHIF.txt","SH1.txt","656565")==0); //дешифрование
    CHECK(Correct("SH1.txt","f1_in_correct.txt")==true);//проверка работы дешифрования
}
TEST_CASE("rignt_copy_Stest","[Copy]")
{
    CHECK(Shifr("empty.txt","SH1.txt","656565")==0); //дешифрование

}
TEST_CASE("double_Scopy_test","[Copy]")
{
    CHECK(Shifr("f1_in_correct.txt","SHIF.txt","656565")==0);
    CHECK(Shifr("SHIF.txt","SH1.txt","656565")==0);
    CHECK(Shifr("SH1.txt","f1_in_correct.txt","656565")==0);
    CHECK(Correct("SH1.txt","f1_in_correct.txt")==false);
}
TEST_CASE("new_shifr_file","[Copy]")
{
    CHECK(Shifr("f1_in_correct.txt","NewFileName.txt","656565")==0);
}