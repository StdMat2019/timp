timp-Технологии и методы программировнаия
======
Этот репозиторий создан в расках учебной дисциплины Технологии и методы прогаммирования

В рамках курса были изучены структуры данных: список, очередь, стек, хеш-табицы с различными способами разрешения коллизий, деревья,графы, а также различные алгориты основанные на этих стрктурах данных.

Самостоятельно реализованные структуры данных:


task4-1 -Стек на основе связного списка. 
-----
Операции: 
* добавление элемента 
* извлечение элемента
* определение количества элементов в стеке
* очистка стека

<https://gitlab.com/StdMat2019/timp/-/tree/main/task4-1>

task4-2 - Бинарное дерево.
-----
Операции:
* добавление элемента по ключу
* удаление элемента по ключу
* поиск элемента по ключу
* поиск минимального и максимального элемента (по ключу).

<https://gitlab.com/StdMat2019/timp/-/tree/main/task4-2>

task4-3 Хеш-таблица с разрешением коллизий с помощью связных списков.
-----
Операции:
* добавление элемента по ключу
* поиск элемента по ключу
* определение количества элементов списка для заданной ячейки хэш-таблицы
* определение общего количества содержащихся элементов.

<https://gitlab.com/StdMat2019/timp/-/tree/main/task4-3>

task4-4 Алгоритм пирамидальной сортировки.
-----

<https://gitlab.com/StdMat2019/timp/-/tree/main/task4-4>

task4-5 Граф неориентированный.
-----
Операции:
* Поиск (обход) в ширину с помощью матрицы смежности из заданной вершины
* Выделение связных компонент.

<https://gitlab.com/StdMat2019/timp/-/tree/main/task4-5>

task4-6 Нахождение кратчайших путей от одной вершины до всех остальных в взвешенных графах. Алгоритм Беллмана-Форда.
-----
<https://gitlab.com/StdMat2019/timp/-/tree/main/task4-6>

Также как каждому заданию написаны тесты с высокой степенью покрытия.
