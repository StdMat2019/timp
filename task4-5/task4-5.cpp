//
// Created by user on 4/22/21.
//
#include "task4-5.h"
#include <map>
Graph::Graph(std::string str)
{
    std::string V = "";
    std::string E = "";
    sizeVertex = 0;
    sizeEdges = 0;
     if(str=="") return;
    for (int i = 0; i < str.size(); ++i)
    {
        if ('a'<=str[i]&& 'z'>=str[i])
        {
            if ( 'a'<=str[i + 1]&& 'z'>=str[i + 1])
            {
                E += str[i];
                E += str[i + 1];
                E+=" ";
                ++i;
                ++sizeEdges;
            }
            else
            {
                V += str[i];
                ++sizeVertex;
            }
        }
    }


    for (int l = 0; l <V.size() ; ++l)
    {
        for (int i = 0; i < V.size(); ++i)
        {
            if(V[i]>V[l])
            {
                std::swap(V[i],V[l]);
            }
        }
    }
    for (int m = 0; 3*m+1 < E.size(); ++m)
    {
        if(E[3*m]>E[3*m+1])
        {
            std::swap(E[3*m],E[3*m+1]);
        }
    }
    for (int i = 0; i < sizeVertex; ++i)
    {
        index.insert(std::make_pair((V[i]),i ));
        indexInverse.insert(std::make_pair(i,(V[i]) ));
    }
    pMatrix = new bool *[sizeVertex];
    for (int j = 0; j < sizeVertex; ++j)
    {
        pMatrix[j] = new bool[sizeVertex];
    }
    for (int k = 0; k < sizeVertex; ++k)
    {
        for (int i = k; i < sizeVertex; ++i)
        {
            pMatrix[k][i] =  FindPair(V[k], V[i], E);
            pMatrix[i][k] = FindPair(V[k], V[i], E);
        }

    }
}

bool Graph::FindPair(char first,char second,std::string E)
{
    for (int i = 0; i <E.size(); ++i)
    {
        if(E[i]==first && E[i+1]==second)
        {
            return true;
        }
    }
    return false;
}

void Graph::Print()
{
    for (int i = 0; i <sizeVertex ; ++i)
    {
        for (int j = 0; j <sizeVertex ; ++j)
        {
            std::cout<< pMatrix[i][j]<<" ";
        }
        std::cout<< std::endl;
    }
}

std::string Graph::BreadthFirstSearch(char Peak)
{
    std::queue<int> temp;//очередь для перебора значений
    std::pair<int,int> peaksList[sizeVertex];//список пройденных вершин
    std::string peaks="";
    for (int k = 0; k <sizeVertex ; ++k)
    {
        peaksList[k]=std::make_pair(k,0);//присвоим всем вершинам метку 0
    }
    if(index.find(Peak)==index.end())
        throw "Попытка обхода несуществующего элемента";
    temp.push(index.at(Peak));//добавление заданнного элемента в очередь обхода
    peaksList[index.at(Peak)].second=1;
    peaks+=Peak;
    peaks+=',';
    while (temp.size()!=0)
    {
        for (int i = 0; i < sizeVertex; ++i)
        {
            if (1==pMatrix[temp.front()][i] &&1!=peaksList[i].second)
                {
                    temp.push(i);
                    peaksList[i].second=1;//новым элементам присваиваем метку
                    peaks+=indexInverse.at(i);//дополняем список вешин которые уже обошли
                    peaks+=',';
                }
        }
        temp.pop();
    }
    peaks.erase(peaks.size()-1);
    return peaks;
}

std::string Graph::CommunityComponent()
{
    if(sizeVertex==0) return "";
    std::queue<int> temp;//очередь для перебора значений
    std::pair<int,int> peaksList[sizeVertex];//список пройденных вершин
    for (int k = 0; k <sizeVertex ; ++k)
    {
        peaksList[k]=std::make_pair(k,0);//присвоим всем вершинам метку 0
    }
    int NumberDot=1;
    for (int l = 0; l < sizeVertex; ++l)
    {

        while (0!=peaksList[l].second)
        {
            ++l;
        }
        if(sizeVertex<=l)
        {
            break;
        }
        temp.push(peaksList[l].first);//добавление заданнного элемента в очередь обхода
        peaksList[l].second= NumberDot;

        while (temp.size()!=0)
        {
            for (int i = 0; i < sizeVertex; ++i)
            {
               if ( 1==pMatrix[temp.front()][i] &&0==peaksList[i].second)
               {
                   temp.push(i);
                  peaksList[i].second= NumberDot;//новым элементам присваиваем метку
                }
            }
        temp.pop();
        }
        ++NumberDot;
    }
    //произведен поиск вершин и расставлены метки

    std::string CommunitiLine="";
    for (int n = 1; n <sizeVertex+1 ; ++n)
    {
        for(int i=0;i<sizeVertex+1;++i)
        {
            if(peaksList[i].second==n)
            {
                {
                    CommunitiLine += indexInverse.at(peaksList[i].first);

                }

            }
        }
        CommunitiLine += ',';
    }
    while (','==CommunitiLine[CommunitiLine.size()-1])
    CommunitiLine.erase(CommunitiLine.end()-1);
    return CommunitiLine;
}

Graph::~Graph()
{
    for (int j = 0; j < sizeVertex; ++j)
    {
        delete  pMatrix[j];
    }
}