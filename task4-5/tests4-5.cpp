//
// Created by user on 4/22/21.
//
#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include <iostream>
#include <string>
#include "task4-5.h"
TEST_CASE("Empty","[Exept_Work]")
{
    Graph a("");
    CHECK_THROWS(a.BreadthFirstSearch('a'));
    CHECK_NOTHROW(a.CommunityComponent());
    CHECK(a.CommunityComponent()=="");
}

TEST_CASE("Without_Edges","[Exept_Work]")
{
    Graph a("a,b,c,d,e,f");
    CHECK(a.BreadthFirstSearch('a')=="a");
    CHECK(a.BreadthFirstSearch('b')=="b");
    CHECK(a.BreadthFirstSearch('c')=="c");
    CHECK(a.CommunityComponent()=="a,b,c,d,e,f");
}

TEST_CASE("Without_Vertex","[Exept_Work]")
{
    Graph a("ac,bd,cs,ds,fe,gf");
    CHECK_THROWS(a.BreadthFirstSearch('a'));
    CHECK(""==a.CommunityComponent());
}

TEST_CASE("Simple_Line","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,ab,bc,cd,de,ef,fg");
    CHECK("a,b,c,d,e,f,g"==a.BreadthFirstSearch('a'));
    CHECK("b,a,c,d,e,f,g"==a.BreadthFirstSearch('b'));
    CHECK("c,b,d,a,e,f,g"==a.BreadthFirstSearch('c'));
    CHECK("d,c,e,b,f,a,g"==a.BreadthFirstSearch('d'));
    CHECK("e,d,f,c,g,b,a"==a.BreadthFirstSearch('e'));
    CHECK("f,e,g,d,c,b,a"==a.BreadthFirstSearch('f'));
    CHECK("g,f,e,d,c,b,a"==a.BreadthFirstSearch('g'));
    CHECK("abcdefg"==a.CommunityComponent());
}

TEST_CASE("Simple_Circle","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,ab,bc,cd,de,ef,fg,ga");
    CHECK("a,b,g,c,f,d,e"==a.BreadthFirstSearch('a'));
    CHECK("b,a,c,g,d,f,e"==a.BreadthFirstSearch('b'));
    CHECK("c,b,d,a,e,g,f"==a.BreadthFirstSearch('c'));
    CHECK("d,c,e,b,f,a,g"==a.BreadthFirstSearch('d'));
    CHECK("e,d,f,c,g,b,a"==a.BreadthFirstSearch('e'));
    CHECK("f,e,g,d,a,c,b"==a.BreadthFirstSearch('f'));
    CHECK("g,a,f,b,e,c,d"==a.BreadthFirstSearch('g'));
    CHECK("abcdefg"==a.CommunityComponent());
}

TEST_CASE("Simple_Radial","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,ab,ac,ad,ae,af,ag");
    CHECK("a,b,c,d,e,f,g"==a.BreadthFirstSearch('a'));
    CHECK("b,a,c,d,e,f,g"==a.BreadthFirstSearch('b'));
    CHECK("c,a,b,d,e,f,g"==a.BreadthFirstSearch('c'));
    CHECK("d,a,b,c,e,f,g"==a.BreadthFirstSearch('d'));
    CHECK("e,a,b,c,d,f,g"==a.BreadthFirstSearch('e'));
    CHECK("f,a,b,c,d,e,g"==a.BreadthFirstSearch('f'));
    CHECK("g,a,b,c,d,e,f"==a.BreadthFirstSearch('g'));
    CHECK("abcdefg"==a.CommunityComponent());
}

TEST_CASE("Simple_Two_Elem","[Normal_Work]")
{
    Graph a("a,b,ab");
    CHECK("a,b"==a.BreadthFirstSearch('a'));
    CHECK("b,a"==a.BreadthFirstSearch('b'));
    CHECK("ab"==a.CommunityComponent());
}

TEST_CASE("Normal_Two_Component","[Normal_Work]")
{
    Graph a("a,b,c,d,e,ab,ac,de");//две компоненты связности
    CHECK("a,b,c"==a.BreadthFirstSearch('a'));
    CHECK("d,e"==a.BreadthFirstSearch('d'));
    CHECK("abc,de"==a.CommunityComponent());
}

TEST_CASE("Normal_Four_Component","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,h,i,k,ab,ac,ce,fg,gh,ik");//4 компоненты связности
    CHECK("a,b,c,e"==a.BreadthFirstSearch('a'));
    CHECK("d"==a.BreadthFirstSearch('d'));
    CHECK("k,i"==a.BreadthFirstSearch('k'));
    CHECK("f,g,h"==a.BreadthFirstSearch('f'));
    CHECK("abce,d,fgh,ik"==a.CommunityComponent());
}

TEST_CASE("InverseVertex_Four_Component","[Normal_Work]")
{
    Graph a("e,i,h,f,g,k,d,c,a,b,,ab,ac,ce,fg,gh,ik");//4 компоненты связности
    CHECK("a,b,c,e"==a.BreadthFirstSearch('a'));
    CHECK("d"==a.BreadthFirstSearch('d'));
    CHECK("k,i"==a.BreadthFirstSearch('k'));
    CHECK("f,g,h"==a.BreadthFirstSearch('f'));
    CHECK("abce,d,fgh,ik"==a.CommunityComponent());
}

TEST_CASE("InverseEdges_Four_Component","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,h,i,k,ba,ca,ec,gf,hg,ki");//4 компоненты связности
    CHECK("a,b,c,e"==a.BreadthFirstSearch('a'));
    CHECK("d"==a.BreadthFirstSearch('d'));
    CHECK("k,i"==a.BreadthFirstSearch('k'));
    CHECK("f,g,h"==a.BreadthFirstSearch('f'));
    CHECK("abce,d,fgh,ik"==a.CommunityComponent());
}

TEST_CASE("Normal_Component","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,h,i,k,ab");
    CHECK("a,b"==a.BreadthFirstSearch('a'));
    CHECK("d"==a.BreadthFirstSearch('d'));
    CHECK("k"==a.BreadthFirstSearch('k'));
    CHECK("f"==a.BreadthFirstSearch('f'));
    CHECK("ab,c,d,e,f,g,h,i,k"==a.CommunityComponent());
}

TEST_CASE("Normal_Component2","[Normal_Work]")
{
    Graph a("a,b,c,d,e,f,g,h,i,k,ab,dc,ef,gh,ik");
    CHECK("a,b"==a.BreadthFirstSearch('a'));
    CHECK("d,c"==a.BreadthFirstSearch('d'));
    CHECK("k,i"==a.BreadthFirstSearch('k'));
    CHECK("f,e"==a.BreadthFirstSearch('f'));
    CHECK("ab,cd,ef,gh,ik"==a.CommunityComponent());
}

TEST_CASE("Inverse_Component2","[Normal_Work]")
{
    Graph a("k,i,h,g,f,e,d,c,a,b,ab,dc,ef,gh,ik");
    CHECK("a,b"==a.BreadthFirstSearch('a'));
    CHECK("d,c"==a.BreadthFirstSearch('d'));
    CHECK("k,i"==a.BreadthFirstSearch('k'));
    CHECK("f,e"==a.BreadthFirstSearch('f'));
    CHECK("ab,cd,ef,gh,ik"==a.CommunityComponent());
}

TEST_CASE("Full_Graph4_Component","[Normal_Work]")
{
    Graph a("q,w,e,r,wq,qe,rq,we,rw,er");
    CHECK("q,e,r,w"==a.BreadthFirstSearch('q'));
    CHECK("w,e,q,r"==a.BreadthFirstSearch('w'));
    CHECK("e,q,r,w"==a.BreadthFirstSearch('e'));
    CHECK("r,e,q,w"==a.BreadthFirstSearch('r'));
    CHECK("eqrw"==a.CommunityComponent());
}

TEST_CASE("Full_Graph6_Component","[Normal_Work]")
{
    Graph a("a,d,b,e,c,f,ad,ae,af,ab,ac,bd,be,bf,bc,cd,cf,ce,de,df,ef");
    CHECK("a,b,c,d,e,f"==a.BreadthFirstSearch('a'));
    CHECK("b,a,c,d,e,f"==a.BreadthFirstSearch('b'));
    CHECK("c,a,b,d,e,f"==a.BreadthFirstSearch('c'));
    CHECK("f,a,b,c,d,e"==a.BreadthFirstSearch('f'));
    CHECK("abcdef"==a.CommunityComponent());
}

TEST_CASE("UnNormal_Graph6_Component","[Exept_Work]")
{
    Graph a("a,d,b,e,c,f,af,bd,cf,ef");
    CHECK("a,f,c,e"==a.BreadthFirstSearch('a'));
    CHECK("b,d"==a.BreadthFirstSearch('b'));
    CHECK("c,f,a,e"==a.BreadthFirstSearch('c'));
    CHECK("f,a,c,e"==a.BreadthFirstSearch('f'));
    CHECK("acef,bd"==a.CommunityComponent());
}

TEST_CASE("Normal_Graph6_Component","[Normal_Work]")
{
    Graph a("a,d,b,e,c,f,af,bd,cf,ef,af,bd,cf,ef");
    CHECK("a,f,c,e"==a.BreadthFirstSearch('a'));
    CHECK("b,d"==a.BreadthFirstSearch('b'));
    CHECK("c,f,a,e"==a.BreadthFirstSearch('c'));
    CHECK("f,a,c,e"==a.BreadthFirstSearch('f'));
    CHECK("acef,bd"==a.CommunityComponent());
}

TEST_CASE("Normal_Graph_Component","[Normal_Work]")
{
    Graph a("q,e,t,u,o,i,y,r,a,g,s,x,b,d,f,k,h,v,m,z,l,p,qt,to,ox,od,oq,eu,ui,sf,kh,vm,mz,lp,bp");
    CHECK("d,o,q,t,x"==a.BreadthFirstSearch('d'));
    CHECK("b,p,l"==a.BreadthFirstSearch('b'));
    CHECK("a"==a.BreadthFirstSearch('a'));
    CHECK("f,s"==a.BreadthFirstSearch('f'));
    CHECK("a,blp,doqtx,eiu,fs,g,hk,mvz,r,y"==a.CommunityComponent());
}