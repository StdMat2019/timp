//
// Created by user on 4/22/21.
//
#ifndef TEST_EMPTY_TASK4_5_H
#define TEST_EMPTY_TASK4_5_H
/*Граф неориентированный.
 * Поиск (обход) в ширину с помощью матрицы смежности из заданной вершины.
 * Выделение связных компонент.*/
#include <iostream>
#include <map>
#include <queue>
class Graph
{
private:
    std::map<int, char> index;
    std::map<int, char> indexInverse;
    bool **pMatrix;
    int sizeVertex;
    int sizeEdges;
    bool FindPair(char first,char second,std::string E);
public:
    Graph(std::string str);
    ~Graph();
    void Print();
    std::string BreadthFirstSearch(char Peak);
    std::string CommunityComponent();
};
#endif //TEST_EMPTY_TASK4_5_H
