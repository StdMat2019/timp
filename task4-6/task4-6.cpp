//
// Created by user on 4/27/21.
//
#include "task4-6.h"
#include <algorithm>
GRAPH::GRAPH(std::string str)
{
    sizeVertex = 0;
    sizeEdges = 0;
    std::string V = "";
    std::string E = "";
    for (int i = 0; i < str.size(); ++i)// разбиение входной строки на множество вершин и множествое ребер
    {
        if ('a'<=str[i]&& 'z'>=str[i])
        {
            if ( 'a'<=str[i + 1]&& 'z'>=str[i + 1])
            {
                while (','!=str[i]&&'\0'!=str[i])
                {
                    E+=str[i];
                    ++i;
                }
                E+=" ";
                ++sizeEdges;
            }
            else
            {
                V += str[i];
                ++sizeVertex;
            }
        }
    }

    for (int l = 0; l <V.size() ; ++l)//упорядочивание вершин графа
    {
        for (int i = 0; i < V.size(); ++i)
        {
            if(V[i]>V[l])
            {
                std::swap(V[i],V[l]);
            }
        }
    }

    for (int i = 0; i < sizeVertex; ++i)//построение отбражения множества вершин в индексы массива
    {
        index.insert(std::make_pair((V[i]),i ));
        indexInverse.insert(std::make_pair(i,(V[i]) ));
    }

    Edges= new edge [sizeEdges];
    if(str=="") return ;
    int temp=0;
    int numV=0;
    for (int i = 0; i <E.size() ; i+=3) //преобразование строки в массив вершин
    {
        Edges[numV].v=index.at(E[i]);
        Edges[numV].u=index.at(E[i+1]);
        if(E[i+2]=='-')//условие при отрицательной длине ребра
        {
            while (E[i+3] != ' ')
            {
                temp += (E[i + 3]-'0');
                temp *= 10;
                ++i;
            }
            temp /= 10;
            temp*=(-1);
            ++i;
        }
        else {
            while (E[i+2] != ' ') {
                temp += E[i + 2]-'0';
                temp *= 10;
                ++i;
            }
            temp /= 10;
        }
        Edges[numV].w=temp;
        temp=0;
        ++numV;
    }
}

bool GRAPH::BellmanFord(char peak)
{
    if(index.find(peak)==index.end())
        return  1;
    int Peak=index.at(peak);
    dist=new std::pair<int ,std::string> [sizeVertex];

    for (int j = 0; j < sizeVertex; ++j)
    {
        dist[j].first=INT_MAX/2;
    }

    dist[Peak].second=peak;
    dist[Peak].first=0;//маршрут элемента к самому себе

    for (int i = 0; i <sizeVertex-1 ; ++i)
    {
        for (int j = 0; j < sizeEdges; ++j)
        {
            if(dist[Edges[j].u].first>dist[Edges[j].v].first+abs(Edges[j].w))//условие того что новый маршрут короче предыдущего
            {

                    dist[Edges[j].u].first=dist[Edges[j].v].first+Edges[j].w;
                    dist[Edges[j].u].second="";//необходимо для перезаписывания пути при смене кратчайшего расстояния
                    dist[Edges[j].u].second += dist[Edges[j].v].second;
                    dist[Edges[j].u].second += indexInverse.at(Edges[j].u);

            }

        }
    }

    return 0;
}