//
// Created by user on 4/27/21.
//
#define CATCH_CONFIG_MAIN
#include "../catch.hpp"
#include <iostream>
#include <string>
#include "task4-6.h"

TEST_CASE("Simple_Test","[Normal_Work]")
{
    GRAPH a("a,b,c,ab43,ac-4");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="ab");
    CHECK(a.dist[a.index.at('b')].first==43);
    CHECK(a.dist[a.index.at('c')].second=="ac");
    CHECK(a.dist[a.index.at('c')].first==-4);

}

TEST_CASE("Simple_Test2","[Normal_Work]")
{
    GRAPH a("a,b,c,ab43,ac-4,cb34");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="acb");
    CHECK(a.dist[a.index.at('b')].first==30);
    CHECK(a.dist[a.index.at('c')].second=="ac");
    CHECK(a.dist[a.index.at('c')].first==-4);

}

TEST_CASE("Simple_Test3","[Normal_Work]")
{
    GRAPH a("a,b,c,d,e,f,ab43,ac-4");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="ab");
    CHECK(a.dist[a.index.at('b')].first==43);
    CHECK(a.dist[a.index.at('c')].second=="ac");
    CHECK(a.dist[a.index.at('c')].first==-4);
    CHECK(a.dist[a.index.at('d')].second=="");
    CHECK(a.dist[a.index.at('d')].first==INT_MAX/2);

}

TEST_CASE("Test4","[Normal_Work]")
{
    GRAPH a("a,b,c,d,e,f,ab3,be34,ec10,ed7,ef5,fd18,dc1,af30");
    CHECK(a.BellmanFord('a')==0);//дистанции из вершины а
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="ab");
    CHECK(a.dist[a.index.at('b')].first==3);
    CHECK(a.dist[a.index.at('c')].second=="abedc");
    CHECK(a.dist[a.index.at('c')].first==45);
    CHECK(a.dist[a.index.at('d')].second=="abed");
    CHECK(a.dist[a.index.at('d')].first==44);
    CHECK(a.dist[a.index.at('f')].second=="af");
    CHECK(a.dist[a.index.at('f')].first==30);
    CHECK(a.dist[a.index.at('e')].second=="abe");
    CHECK(a.dist[a.index.at('e')].first==37);

    CHECK(a.BellmanFord('e')==0);//перезаписывание дистанций на дистанции от е
    CHECK(a.dist[a.index.at('a')].second=="");
    CHECK(a.dist[a.index.at('a')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('b')].second=="");
    CHECK(a.dist[a.index.at('b')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('c')].second=="edc");
    CHECK(a.dist[a.index.at('c')].first==8);
    CHECK(a.dist[a.index.at('d')].second=="ed");
    CHECK(a.dist[a.index.at('d')].first==7);
    CHECK(a.dist[a.index.at('f')].second=="ef");
    CHECK(a.dist[a.index.at('f')].first==5);
    CHECK(a.dist[a.index.at('e')].second=="e");
    CHECK(a.dist[a.index.at('e')].first==0);

    CHECK(a.BellmanFord('c')==0);//перезаписывание дистанций на дистанции от с
    CHECK(a.dist[a.index.at('a')].second=="");
    CHECK(a.dist[a.index.at('a')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('b')].second=="");
    CHECK(a.dist[a.index.at('b')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('c')].second=="c");
    CHECK(a.dist[a.index.at('c')].first==0);
    CHECK(a.dist[a.index.at('d')].second=="");
    CHECK(a.dist[a.index.at('d')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('f')].second=="");
    CHECK(a.dist[a.index.at('f')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('e')].second=="");
    CHECK(a.dist[a.index.at('e')].first==INT_MAX/2);

}

TEST_CASE("Simple_Line_Test5","[Normal_Work]")
{
    GRAPH a("a,b,c,d,e,f,ab45,bc14,cd-5,de-4,ef40");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="ab");
    CHECK(a.dist[a.index.at('b')].first==45);
    CHECK(a.dist[a.index.at('c')].second=="abc");
    CHECK(a.dist[a.index.at('c')].first==59);
    CHECK(a.dist[a.index.at('d')].second=="abcd");
    CHECK(a.dist[a.index.at('d')].first==54);
    CHECK(a.dist[a.index.at('e')].second=="abcde");
    CHECK(a.dist[a.index.at('e')].first==50);
    CHECK(a.dist[a.index.at('f')].second=="abcdef");
    CHECK(a.dist[a.index.at('f')].first==90);

}

TEST_CASE("Simple_InverseLine_Test5","[Normal_Work]")
{
    GRAPH a("a,b,c,d,e,f,ba45,cb14,dc-5,ed-4,fe40");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="");
    CHECK(a.dist[a.index.at('b')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('c')].second=="");
    CHECK(a.dist[a.index.at('c')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('d')].second=="");
    CHECK(a.dist[a.index.at('d')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('e')].second=="");
    CHECK(a.dist[a.index.at('e')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('f')].second=="");
    CHECK(a.dist[a.index.at('f')].first==INT_MAX/2);
}

TEST_CASE("Exept_Simple_Test","[Exept_Work]")
{
    GRAPH a("a");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
}

TEST_CASE("Exept_Negative_Length_Cycle_Test","[Exept_Work]")
{
    GRAPH a("a,b,c,d,ab-4,bc-3,cd6,da-1,ad23,");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="ab");
    CHECK(a.dist[a.index.at('b')].first==-4);
    CHECK(a.dist[a.index.at('c')].second=="abc");
    CHECK(a.dist[a.index.at('c')].first==-7);
    CHECK(a.dist[a.index.at('d')].second=="abcd");
    CHECK(a.dist[a.index.at('d')].first==-1);
}

TEST_CASE("Exept_Test","[Exept_Work]")
{
    GRAPH a("");
    CHECK(a.BellmanFord('a')==1);
}

TEST_CASE("Exept_Test_Without_Edges","[Exept_Work]")
{
    GRAPH a("a,b,c,d");
    CHECK(a.BellmanFord('a')==0);
    CHECK(a.dist[a.index.at('a')].second=="a");
    CHECK(a.dist[a.index.at('a')].first==0);
    CHECK(a.dist[a.index.at('b')].second=="");
    CHECK(a.dist[a.index.at('b')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('c')].second=="");
    CHECK(a.dist[a.index.at('c')].first==INT_MAX/2);
    CHECK(a.dist[a.index.at('d')].second=="");
    CHECK(a.dist[a.index.at('d')].first==INT_MAX/2);
}