//
// Created by user on 4/27/21.
//
//Вариант 1. Нахождение кратчайших путей от одной вершины до всех остальных в взвешенных графах. Алгоритм Беллмана-Форда.
#ifndef TEST_EMPTY_TASK4_6_H
#define TEST_EMPTY_TASK4_6_H
#include <iostream>
#include <map>
#include <climits>

struct edge //вспомогательная структура для работы с вершинами графа
{
    int u;
    int v;
    int w;
};

class GRAPH
{
private:
    std::map<int, char> indexInverse;
    int sizeVertex;
    int sizeEdges;
    edge *Edges;//множество ребер
public:
    GRAPH(std::string str);
    ~GRAPH(){delete[] Edges; delete[] dist;};
    std::map<int, char> index;//отображение вершин в в индексы
    bool BellmanFord(char peak);
    std::pair<int,std::string> *dist;//указатель на массив в котором хрантся результаты работы алгоритма Беллмана-Форда
};
#endif //TEST_EMPTY_TASK4_6_H
