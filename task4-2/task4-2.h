/*Бинарное дерево.
Операции:
добавление элемента по ключу;
удаление элемента по ключу;
поиск элемента по ключу;
поиск минимального и максимального элемента (по ключу);
*/
 #include <iostream>
struct Node
{
    int key;
    std::string value;
    Node *left;
    Node *right;
    Node *p;
    Node(int key,std::string value);
    Node();
    ~Node();
};
class BinaryTree
{
private:
    Node *root;
    int sizeTree;
    int HighTree(Node *N);
    bool CorrectElement(Node *N);
    Node *Minimum(Node *x);
    Node *Maximum(Node *x);
public:
    BinaryTree();
    ~BinaryTree();
    void Push(int key, std::string value);
    void Remove(int key);
    Node *Search(int key);
    Node *Maximum();
    Node *Minimum();
    int HighBinaryTree();
    int SizeTree();
    bool CheckCorrect();
    void Clear();
    void RecursiveDelete(Node* value);
};
