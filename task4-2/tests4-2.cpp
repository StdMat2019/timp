
#define CATCH_CONFIG_MAIN
#include "task4-2.h"
#include "../catch.hpp"
#include <ctime>
#include <iostream>
#include <string>
TEST_CASE("Test_Node_Constructor","[Node]") //корректность конструкторов
{
    srand(time(0));
    int a=rand()%100;
    Node Simple(a,"simple value");
    CHECK(Simple.key==a);
    CHECK(Simple.value=="simple value");
    CHECK(nullptr==Simple.left);
    CHECK(nullptr==Simple.right);
    CHECK(nullptr==Simple.p);
    Node *Default=new Node();
    CHECK(nullptr==Default->right);
    CHECK(nullptr==Default->left);
    CHECK(nullptr==Default->p);
    delete Default;
}

TEST_CASE("Test_Normal_BinaryTree","[Normal_Work]")
{
    srand(time(0));
    int a=rand()%100;
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(a,"Value"));
    CHECK_NOTHROW(Tree.Minimum());
    CHECK_NOTHROW(Tree.Maximum());
    CHECK_NOTHROW(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.HighBinaryTree());
    CHECK_NOTHROW(Tree.SizeTree());
    CHECK_NOTHROW(Tree.Search(a));
    CHECK_NOTHROW(Tree.Remove(a));
}

TEST_CASE("Test_Normal_BinaryTree_Push_Size","[Normal_Work]") {
    const int size = 10;
    int arr[size];
    for (int i = 0; i < size; ++i)
    {
        arr[i] = i*40;
    }
    BinaryTree Tree;
    for (int i = 0; i < size; ++i)
    {
        CHECK_NOTHROW(Tree.Push(arr[i],"Value"));
        CHECK(Tree.SizeTree()==i+1);
    }
}
TEST_CASE("Test_Normal_BinaryTree_Push_High","[Normal_Work]") {
    srand(time(0));
    const int size = 10;
    BinaryTree Tree;
    for (int i = 0; i < size; ++i)
    {
        CHECK_NOTHROW(Tree.Push(i,"Value"));
        CHECK(Tree.HighBinaryTree()==i+1);
        CHECK(Tree.Maximum()->key==i);
        CHECK(Tree.Minimum()->key==0);
    }
}

TEST_CASE("Test_Normal_BinaryTree_Push_Value","[Normal_Work]") {
    srand(time(0));
    BinaryTree Tree;
    const int size = 10;
    std::string arrValue[size];
    for (int i = 0; i < size; ++i)
    {
        arrValue[i]=(char)('a' +rand() %('a'-'z') );
        arrValue[i]+=(char)('a' +rand() %('a'-'z') );
        arrValue[i]+=(char)('a' +rand() %('a'-'z') );
        arrValue[i]+=(char)('a' +rand() %('a'-'z') );
        CHECK_NOTHROW(Tree.Push(i,arrValue[i]));
    }
    for (int i = 0; i < size; ++i)
    {
        CHECK(Tree.Search(i)->value==arrValue[i]);
    }

}

TEST_CASE("Test_Normal_BinaryTree_Push_Search","[Normal_Work]") {
    srand(time(0));
    BinaryTree Tree;
    const int size = 10;
    std::string arrValue[size];
    int arrKey[size];
    for (int i = 0; i < size; ++i)
    {
        arrKey[i]=i*10;
        arrValue[i]=(char)('a' +rand() %('a'-'z') );
        arrValue[i]+=(char)('a' +rand() %('a'-'z') );
        arrValue[i]+=(char)('a' +rand() %('a'-'z') );
        arrValue[i]+=(char)('a' +rand() %('a'-'z') );
        CHECK_NOTHROW(Tree.Push(arrKey[i],arrValue[i]));
        CHECK(Tree.CheckCorrect());
    }
    for (int i = 0; i < size; ++i)
    {
        CHECK(Tree.Search(arrKey[i])->value==arrValue[i]);

        CHECK(Tree.Search(arrKey[i])->key==arrKey[i]);
    }

}


TEST_CASE("Test_Normal_BinaryTree_Remove_ALL_Variations","[Normal_Work]") {
    srand(time(0));
    int arrKey[10];
    arrKey[0]=1000*(rand()%50);
    BinaryTree Tree;
    const int size = 10;
    for(int i=0;2*i+1<size;++i)
    {
        arrKey[2*i]=arrKey[i]/2;
        arrKey[2*i+1]=(arrKey[i]*3)/2;
    }
    for (int j = 0; j <size ; ++j) {
        CHECK_NOTHROW(Tree.Push(arrKey[j],"CheckRemove"));
        CHECK(Tree.CheckCorrect());
       // CHECK(j+1==Tree.SizeTree());

    }
    CHECK(9==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[0]));
    CHECK(Tree.CheckCorrect());
    CHECK(8==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[2]));
    CHECK(Tree.CheckCorrect());
    CHECK(7==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[6]));
    CHECK(Tree.CheckCorrect());
    CHECK(6==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[3]));
    CHECK(Tree.CheckCorrect());
    CHECK(5==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[8]));
    CHECK(Tree.CheckCorrect());
    CHECK(4==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[9]));
    CHECK(Tree.CheckCorrect());
    CHECK(3==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[7]));
    CHECK(Tree.CheckCorrect());
    CHECK(2==Tree.SizeTree());
    CHECK_NOTHROW(Tree.Remove(arrKey[1]));
    CHECK(Tree.CheckCorrect());
    CHECK(1==Tree.SizeTree());
    CHECK_THROWS(Tree.Remove(arrKey[4]));
    CHECK(Tree.CheckCorrect());
}

TEST_CASE("Test_Exept_BinaryTree_Clean","[Exept_Work]") {
    BinaryTree Tree;
    CHECK(0==Tree.HighBinaryTree());
    CHECK(0==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.HighBinaryTree());
    CHECK_NOTHROW(Tree.SizeTree());
    CHECK_NOTHROW(Tree.CheckCorrect());
    CHECK_THROWS(Tree.Minimum());
    CHECK_THROWS(Tree.Maximum());
    CHECK_THROWS(Tree.Search(1));
    CHECK_THROWS(Tree.Remove(1));
}
TEST_CASE("Test_Exept_BinaryTree_Clean_Push","[Exept_Work]") {
    BinaryTree Tree;

    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK(123==Tree.Search(123)->key);
    CHECK("newValue" ==Tree.Search(123)->value);
    CHECK(1==Tree.HighBinaryTree());
    CHECK(1==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.SizeTree());
    CHECK_NOTHROW(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Minimum());
    CHECK_NOTHROW(Tree.Maximum());
    CHECK_NOTHROW(Tree.Remove(123));
    CHECK_THROWS(Tree.Search(123));
    CHECK(0==Tree.HighBinaryTree());
    CHECK(0==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
}
TEST_CASE("Test_Exept_BinaryTree_Clean_Push_Search","[Exept_Work]") {
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK_NOTHROW(Tree.Remove(123));
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK(1233==Tree.Search(1233)->key);
    CHECK("secondValue" ==Tree.Search(1233)->value);
    CHECK(1==Tree.HighBinaryTree());
    CHECK(1==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Remove(1233));
    CHECK_NOTHROW(Tree.SizeTree());
    CHECK_NOTHROW(Tree.CheckCorrect());
    CHECK_THROWS(Tree.Minimum());
    CHECK_THROWS(Tree.Maximum());
    CHECK_THROWS(Tree.Search(123));
    CHECK_THROWS(Tree.Search(1233));
    CHECK(0==Tree.HighBinaryTree());
    CHECK(0==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());

}
TEST_CASE("Test_Exept_BinaryTree_Clean_Push_Clean_Push_Search","[Exept_Work]") {
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK_NOTHROW(Tree.Remove(123));
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK(1233==Tree.Search(1233)->key);
    CHECK("secondValue" ==Tree.Search(1233)->value);
    CHECK(1==Tree.HighBinaryTree());
    CHECK(1==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Remove(1233));
    CHECK_NOTHROW(Tree.SizeTree());
    CHECK_NOTHROW(Tree.CheckCorrect());
    CHECK_THROWS(Tree.Minimum());
    CHECK_THROWS(Tree.Maximum());
    CHECK_THROWS(Tree.Search(123));
    CHECK_THROWS(Tree.Search(1233));
    CHECK(0==Tree.HighBinaryTree());
    CHECK(0==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
}
TEST_CASE("Test_Exept_BinaryTree_Uncorrect_Search","[Exept_Work]") {
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Push(124,"thirdValue"));
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Push(127,"fourthValue"));
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Push(12143,"fifthValue"));
    CHECK(Tree.CheckCorrect());
    CHECK(Tree.Search(1234)== nullptr);
    CHECK(Tree.CheckCorrect());
    CHECK(Tree.Search(1232)== nullptr);
    CHECK(Tree.CheckCorrect());
    CHECK(Tree.Search(0)== nullptr);
    CHECK(Tree.CheckCorrect());
    CHECK(Tree.Search(1)== nullptr);
    CHECK(Tree.CheckCorrect());
    CHECK(Tree.Search(-1)== nullptr);
    CHECK(Tree.CheckCorrect());

}

TEST_CASE("Test_Exept_BinaryTree_Two_Push_Search","[Exept_Work]") {
    BinaryTree Tree;

    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK_NOTHROW(Tree.Push(123,"thirdValue"));
    CHECK_NOTHROW(Tree.Push(1233,"fourthValue"));
    CHECK_NOTHROW(Tree.Push(12143,"fifthValue"));
    CHECK(Tree.CheckCorrect());
    CHECK(3==Tree.SizeTree());
    CHECK(123==Tree.Minimum()->key);
    CHECK("thirdValue"==Tree.Minimum()->value);
    CHECK(12143==Tree.Maximum()->key);
    CHECK("fifthValue"==Tree.Maximum()->value);
    CHECK("fourthValue"==Tree.Search(1233)->value);
    CHECK("thirdValue"==Tree.Search(123)->value);

}
TEST_CASE("Test_Exept_BinaryTree_Two_Remove_Search","[Exept_Work]") {
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK_NOTHROW(Tree.Push(1235,"thirdValue"));
    CHECK_NOTHROW(Tree.Push(122,"fourthValue"));
    CHECK_NOTHROW(Tree.Push(12143,"fifthValue"));
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Remove(123));
    CHECK_THROWS(Tree.Remove(123));
    CHECK_NOTHROW(Tree.Remove(1233));
    CHECK_THROWS(Tree.Remove(1233));
    CHECK(3==Tree.SizeTree());
    CHECK(122==Tree.Minimum()->key);
    CHECK("fourthValue"==Tree.Minimum()->value);
    CHECK(12143==Tree.Maximum()->key);
    CHECK("fifthValue"==Tree.Maximum()->value);
}
TEST_CASE("Test_Exept_BinaryTree_Two_Search","[Exept_Work]") {
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK_NOTHROW(Tree.Push(1235,"thirdValue"));
    CHECK_NOTHROW(Tree.Push(122,"fourthValue"));
    CHECK_NOTHROW(Tree.Push(12143,"fifthValue"));
    CHECK(Tree.CheckCorrect());
    CHECK("secondValue"==Tree.Search(1233)->value);
    CHECK("newValue"==Tree.Search(123)->value);
    CHECK("secondValue"==Tree.Search(1233)->value);
    CHECK("newValue"==Tree.Search(123)->value);
    CHECK(5==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK(122==Tree.Minimum()->key);
    CHECK("fourthValue"==Tree.Minimum()->value);
    CHECK(12143==Tree.Maximum()->key);
    CHECK("fifthValue"==Tree.Maximum()->value);
}
TEST_CASE("Test_Exept_BinaryTree_Clean_AfterPush","[Exept_Work]") {
    BinaryTree Tree;
    CHECK_NOTHROW(Tree.Push(123,"newValue"));
    CHECK_NOTHROW(Tree.Push(1233,"secondValue"));
    CHECK_NOTHROW(Tree.Push(1235,"thirdValue"));
    CHECK_NOTHROW(Tree.Push(122,"fourthValue"));
    CHECK_NOTHROW(Tree.Push(12143,"fifthValue"));
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Clear());
    CHECK(0==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK_NOTHROW(Tree.Push(10,"new"));
    CHECK_NOTHROW(Tree.Push(20,"second"));
    CHECK_NOTHROW(Tree.Push(5,"third"));
    CHECK(Tree.CheckCorrect());
    CHECK("third"==Tree.Search(5)->value);
    CHECK("new"==Tree.Search(10)->value);
    CHECK("second"==Tree.Search(20)->value);
    CHECK(Tree.Search(123)== nullptr);
    CHECK(3==Tree.SizeTree());
    CHECK(Tree.CheckCorrect());
    CHECK(5==Tree.Minimum()->key);
    CHECK(20==Tree.Maximum()->key);
    CHECK("second"==Tree.Maximum()->value);
}
