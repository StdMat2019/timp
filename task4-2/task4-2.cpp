#include "task4-2.h"
#include <cmath>
Node::Node(int key, std::string value)
{
 left= nullptr;
 right= nullptr;
 p= nullptr;
 this->key=key;
 this->value=value;
}
Node::Node()
{
    left= nullptr;
    right= nullptr;
    p= nullptr;
}
Node::~Node()
{

    left= nullptr;
    right= nullptr;
    p= nullptr;
    key=0;
    value= "";

}




BinaryTree::BinaryTree()
{
 root= nullptr;
 sizeTree=0;
}
void BinaryTree::Push(int key, std::string value)
{

    Node *temp = new Node(key, value);
    Node *iterParent= nullptr;
    Node *iter = root;
    while (nullptr != iter) //Находим место куда нужно вставить элемент
    {
        iterParent = iter;
        if (temp->key > iter->key)
        {
            iter = iter->right;
        }
        else {
            if (temp->key < iter->key) {
                iter = iter->left;
            } else
            if (temp->key == iter->key)
                break;
        }
    }
    temp->p = iterParent; //устанавливаем указатель для элемента котрый небходимо вствить
    if (nullptr == iterParent)//устанавливаем указалель от родительского элемента к дочернему
    {
        root = temp;
    } else
        {
        if (iterParent->key > temp->key)
        {
            iterParent->left = temp;
        }
        else
        {
            if (iterParent->key < temp->key)
            {
                iterParent->right = temp;
            } else
                {
                iterParent->value=temp->value;
                    --sizeTree;
                }
        }
    }
     ++sizeTree;
    temp= nullptr;


}

Node *BinaryTree::Search(int key)
{
    //try {
        Node *iter = root;
        Node *iterParent = nullptr;
        if(nullptr==iter) throw "Попытка поиска в пустом дереве";
        while (iter->key != key) {
            iterParent = iter;
            if (iter->key > key) //прохождение по дереву и нахождение предполагаемого места нахожения элемента
            {
                iter = iter->left;
            } else if (iter->key < key) {
                iter = iter->right;
            }
            if(iter== nullptr)
                return iter;
        }
        return iter;
    /*}
    catch(const char *a)
    {
        std::cout <<"Значение по запрашиваемому ключу не найдено\n ";
    }
    catch(const int a)
    {
        std::cout <<"Попытка поиска в пустом дереве\n ";
    }*/
}
Node *BinaryTree::Minimum(Node *x)//минимум начиная с определенного участка дерева
{
    //try {
        if(nullptr==x)
        {
            throw "В дереве не существует элементов";
        }
        Node *iter = x;
        while (iter->left != nullptr) {
            iter = iter->left;
        }
        return iter;
   /* }
    catch(const char *)
    {
        std::cout <<"В дереве не существует элементов\n";
    }*/
}
Node *BinaryTree::Maximum(Node *x)//минимум начиная с определенного участка дерева
{
    //try {
        if(nullptr==x)
        {
            throw "В дереве не существует элементов";
        }
        Node *iter = x;
        while (iter->right != nullptr) {
            iter = iter->right;
        }
        return iter;
  /*  }
    catch(const char *)
    {
        std::cout <<"В дереве не существует элементов\n";
    }*/
}
Node* BinaryTree::Maximum()
{
    return this->Maximum(this->root);
}

Node* BinaryTree::Minimum()
{
    return this->Minimum(this->root);
}
int BinaryTree::SizeTree()
{
    return sizeTree;
}
int BinaryTree::HighTree(Node *N) //функция рекурсивного определения высоты начиная с определенного узла
{
    if ( nullptr==N )
    {
        return 0;
    }
    else
    {
        return 1+std::max(HighTree(N->left),HighTree(N->right));
    }
}
int BinaryTree::HighBinaryTree()
{
    return HighTree(root);
}
bool BinaryTree::CorrectElement(Node *N)
{
    if(HighBinaryTree()==0)
    {
        return 1;
    }
    if(nullptr==N->right&&nullptr==N->left)
    {
        return 1;
    }
    if(nullptr==N->left)
    {
        return N->key<N->right->key&&CorrectElement(N->right);
    }
    else
    {
        if(nullptr==N->right)
        {
            return N->key>N->left->key&&CorrectElement(N->left);
        }
        else
        {
            return N->key>N->left->key&&CorrectElement(N->left)&&N->key<N->right->key&&CorrectElement(N->right);
        }
    }

}
bool BinaryTree::CheckCorrect()
{
    return CorrectElement(root);
}
void BinaryTree::Remove(int key)
{
//try{
    Node *iter;
    if (nullptr==Search(key))  //поиск и проверка существовния элемента по заданному ключу
    {
        throw "Клоч не найден";
    }
        iter = Search(key);
        if(iter->p== nullptr)
        {
            if (nullptr == iter->left && nullptr == iter->right) //у нет корня дочерних элементов
            {
                delete root;
                root = nullptr;
                --sizeTree;

                return;
            }
            if (nullptr == iter->left)  //у корня элемента отсутствует левый дочерний узел
            {
                iter->right->p = nullptr;
                delete root;
                root = iter->right;
                --sizeTree;
                return;
            }
            else
            {
                if (nullptr == iter->right) //у корня элемента отсутствует правый дочерний узел
                {
                    iter->left->p = nullptr;
                    delete root;
                    root = iter->left;
                    --sizeTree;
                    return;
                }
            }
        }
        if (nullptr == iter->left && nullptr == iter->right) //нет дочерних элементов
        {
            if (iter->p->key > iter->key)
            {
                iter->p->left = nullptr;

                delete iter;
            }
            else
            {
                iter->p->right = nullptr;
                delete iter;
            }
            --sizeTree;
            return;
        }
        if (nullptr == iter->left)  //у удаляемого элемента отсутствует левый дочерний узел
        {
            if (iter->p->key > iter->key)
            {
                iter->p->left = iter->right;
                iter->right->p = iter->p;

            }
            else
            {
                iter->p->right = iter->right;
                iter->right->p = iter->p;

            }
            delete iter;
            --sizeTree;
            return;
        }
        else
        if (nullptr == iter->right) //у удаляемого элемента отсутствует правый дочерний узел
        {
            if (iter->p->key > iter->key)
            {
                iter->p->left = iter->left;
                iter->left->p = iter->p;
                delete iter;
            }
            else
            {
                iter->p->right = iter->left;
                iter->left->p = iter->p;
                delete iter;
            }
            --sizeTree;
            return;
        }
        else        //удаляемый элемент имеет два дочерних узла
        {
            Node *SecondIter;
            Node temp;
            SecondIter = Minimum(iter->right);
            temp.key=SecondIter->key;
            temp.value=SecondIter->value;
            Remove(SecondIter->key);
            iter->key = temp.key;
            iter->value = temp.value;
            return;
        }
   /* }
    catch (const char *)
    {
        std::cout <<"Клоч не найден\n";
    }*/

}

BinaryTree::~BinaryTree()
{
    RecursiveDelete(root);
}


void BinaryTree::Clear() {

    RecursiveDelete(root);
}

void BinaryTree::RecursiveDelete(Node *value) {

    if(nullptr!=value) {
            RecursiveDelete(value->left);
            RecursiveDelete(value->right);

             if(value->left || value->right)
             {
                 value->key=0;
                 value->value="";
                 root= nullptr;
             }
                 --sizeTree;
            delete value;
   // }
    }
}